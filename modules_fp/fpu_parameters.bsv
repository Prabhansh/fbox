// BASED ON INITIAL RETIMING RESULTS FOR I-Class
`define LATENCY_FMA_SP 8
`define STAGES_FMA_SP 7
`define LATENCY_FMA_DP 10
`define STAGES_FMA_DP 9
`define LATENCY_FADD_SP 5
`define STAGES_FADD_SP 4
`define LATENCY_FADD_DP 6
`define STAGES_FADD_DP 5
`define LATENCY_FMUL_SP 7
`define STAGES_FMUL_SP 6
`define LATENCY_FMUL_DP 8
`define STAGES_FMUL_DP 7
`define LATENCY_FDIV_SP  36
`define LATENCY_FDIV_DP 65 
`define LATENCY_FSQRT_SP 67   
`define LATENCY_FSQRT_DP 123 
`define LATENCY_FCONV 4 
`define STAGES_FCONV 3
`define LATENCY_FREST 1
