/* 
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
  with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: Neel Gala
Email id: neelgala@gmail.com
Details:

--------------------------------------------------------------------------------------------------
*/
package tb_float_f_to_i; 
  `include "Logger.bsv"
  import FIFO :: * ;
  import FIFOF :: * ;
  import SpecialFIFOs :: * ;
  import fpu_convert :: * ;
  import RegFile :: * ;
  import fpu_common :: * ;

`ifdef FLEN64
  `define FLEN 64	
  `define a1 51
  `define a2 52
  `define a3 62
  `define a4 63
  `define MOD mk_fpu_convert_dp_to_int_instance
`else
  `define FLEN 32
  `define a1 22
  `define a2 23
  `define a3 30
  `define a4 31
  `define MOD mk_fpu_convert_sp_to_int_instance
`endif

`ifdef OLEN64
  `define OLEN 64
`else
  `define OLEN 32 	
`endif

  (*synthesize*)
  module mktb_float(Empty);
    RegFile#(Bit#(`index_size) , Bit#(`entry_size)) stimulus <- mkRegFileLoad("input.txt", 0, `stim_size-1);
//    let fadd <- mk_fpu_add_sub_sp_instance;
    let fadd <- (`MOD);

    FIFOF#(Tuple2#(Bit#(`OLEN), Bit#(5))) ff_golden_output <- mkSizedFIFOF(2);

    Reg#(Bit#(`index_size)) read_index <- mkReg(0);
    Reg#(Bit#(`index_size)) golden_index <- mkReg(0);

    /*doc:rule: */
    rule rl_pick_stimulus_entry;
      
      let _e = stimulus.sub(read_index);
      Bit#(8) _flags = truncate(_e);
      _e = _e >> 8;
      Bit#(`OLEN) _output = truncate(_e);
      _e = _e >> `OLEN;
      Bit#(`FLEN) _inp1 = truncate(_e);
      let op = FloatingPoint{sign : unpack(_inp1[`a4]), exp: _inp1[`a3:`a2], sfd: _inp1[`a1:0]};
//      `logLevel( tb, 0, $format("TB: Sending inputs[%d]: input : %h, output %h", read_index, op,_output))


/*
		bit lv_overflow = 0;
		bit lv_zero = flags[3];
		bit lv_infinity = flags[1];
        	bit lv_invalid = flags[0] | flags[2];
	        bit lv_denormal = flags[4];
        	bit lv_manzero = |lv_mantissa;
	        bit lv_inexact = 0;



typedef struct {
   Bool invalid_op;
   Bool divide_0;
   Bool overflow;
   Bool underflow;
   Bool inexact;
} Exception deriving (Bits, Eq);

	Bool lv_invalid_op = isSNaN(op) || isQNaN(op);
	Bool lv_divide_0 = (_inp1 == '0 || _inp1 == {1'b1,'0});
	Bool lv_overflow = (&op.exp == 1 && &op.sfd == 0);
	Bool lv_underflow = (op.exp == '0 && op.sfd != '0);
	Bool lv_inexact = False;  */
	Bool a0 = isSNaN(op) || isQNaN(op);
	Bool a3 = (_inp1 == '0 || _inp1 == {1'b1,'0});
	Bool a1 = (&op.exp == 1 && &op.sfd == 0);
	Bool a4 = (op.exp == '0 && op.sfd != '0);
	Bool a2 = False;


//	let e = Exception{invalid_op: lv_invalid_op, divide_0: lv_divide_0,  overflow: lv_overflow, underflow: lv_underflow, inexact: lv_inexact};
	let e = Exception{invalid_op: a4, divide_0: a3,  overflow: a2, underflow: a1, inexact: a0};


      fadd.start(tuple3(op ,`rounding_mode,e),1,1);				//<--------------------------------------------------	
      ff_golden_output.enq(tuple2(_output, truncate(_flags)));
      read_index <= read_index + 1;
      if(read_index == `stim_size-1)
//      if(read_index == 500)
        $finish(0);
    endrule

    /*doc:rule: */
    rule rl_check_output;
//      match {.valid,.out,.flags} = fadd.receive();
      let x = fadd.receive();
      let valid = x.valid;
      let m_out = x.value;
      let m_flags = x.ex;
      if(valid==1) begin
        let {g_out, g_flags} = ff_golden_output.first;
//        Bit#(`FLEN) _out = {pack(out.sign), out.exp, out.sfd};
        ff_golden_output.deq;
	Bit#(64) g_e_out = signExtend(g_out);

	if( m_out != g_e_out) begin
		`logLevel( tb, 0, $format("TB: Outputs mismatch[%d]. G:%h R:%h,flags G: %h, R: %h",golden_index, g_out, m_out,g_flags,m_flags))
		$finish(0);
	end
        else 
	begin
//		`logLevel( tb, 0, $format("TB: Outputs match [%d], g: %h R: %h, flags G: %h, R: %h", golden_index,g_out, m_out,g_flags,m_flags))
        end

        golden_index <= golden_index + 1;
        if( g_flags != pack(m_flags)) begin
//          `logLevel( tb, 0, $format("TB: Flags mismatch. G:%h R:%h", g_flags, m_flags))
//          $finish(0);
        end
      end
    endrule
  endmodule
endpackage

