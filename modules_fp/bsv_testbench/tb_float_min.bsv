/* 
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
  with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: Neel Gala
Email id: neelgala@gmail.com
Details:

--------------------------------------------------------------------------------------------------
*/
package tb_float_min; 
  `include "Logger.bsv"
  import FIFO :: * ;
  import FIFOF :: * ;
  import SpecialFIFOs :: * ;
//  import fpu_common :: * ;
  import fpu_convert :: *;
  import RegFile :: * ;
  import fpu_common :: * ;
//*******************************************************************
import Real              ::*;
import Vector            ::*;
import BUtils            ::*;
import DefaultValue      ::*;
import FShow             ::*;
import GetPut            ::*;
import ClientServer      ::*;
import FIFO              ::*;
import Divide            ::*;
import SquareRoot        ::*;
import FixedPoint        ::*;

//*******************************************************************
`ifdef FLEN64
  `define FLEN 64	
  `define a1 51
  `define a2 52
  `define a3 62
  `define a4 63
  `define e 11
  `define m 52
  `define MOD mk_fpu_min_dp_instance

`else
  `define FLEN 32
  `define a1 22
  `define a2 23
  `define a3 30
  `define a4 31
  `define e 8
  `define m 23  
  `define MOD mk_fpu_min_sp_instance
`endif
  (*synthesize*)
  module mktb_float(Empty);

    RegFile#(Bit#(`index_size) , Bit#(`entry_size)) stimulus <- mkRegFileLoad("input.txt", 0, `stim_size-1);
//    let fadd <- mk_fpu_min_sp_instance;
    let fadd <- (`MOD);

    FIFOF#(Tuple2#(Bit#(`FLEN), Bit#(5))) ff_golden_output <- mkSizedFIFOF(2);

    Reg#(Bit#(`index_size)) read_index <- mkReg(0);
    Reg#(Bit#(`index_size)) golden_index <- mkReg(0);

    /*doc:rule: */
    rule rl_pick_stimulus_entry;
      
      let _e = stimulus.sub(read_index);
      Bit#(8) _flags = truncate(_e);
      _e = _e >> 8;
      Bit#(`FLEN) _output = truncate(_e);
      _e = _e >> `FLEN;
      Bit#(`FLEN) _inp2 = truncate(_e);
      _e = _e >> `FLEN;
      Bit#(`FLEN) _inp1 = truncate(_e);

      let op1 = FloatingPoint{sign : unpack(_inp1[`a4]), exp: _inp1[`a3:`a2], sfd: _inp1[`a1:0]};
      let op2 = FloatingPoint{sign : unpack(_inp2[`a4]), exp: _inp2[`a3:`a2], sfd: _inp2[`a1:0]};
      `logLevel( tb, 0, $format("TB: Sending inputs[%d]: op1:%h op2:%h, output %h", read_index, op1, op2,_output))


//----------------------------------- min_max function testing -------------------------------------------------------------------------
//	let x = min(op1,op2);
	let ans <-fadd.start(tuple2(op1,op2));
	let x = ans.value;
	
	Bit#(5) pack_flags = truncate(_flags);
	Bit#(`FLEN) _out = {pack(x.sign), x.exp, x.sfd};
	
	if( _out != _output) begin
      `logLevel( tb, 0, $format("TB: Sending inputs for mismatch [%d]: op1:%h op2:%h, output %h", read_index, op1, op2,_output))
          `logLevel( tb, 0, $format("TB: Outputs mismatch[%d]. G:%h R:%h, flags G: %h",read_index, _output, _out, pack_flags))
		$finish(0);
        end
        else begin
            `logLevel( tb, 0, $format("TB: Outputs match [%d], g: %h R: %h flags G: %h", read_index,_output, _out,pack_flags))
        end
//        if( pack_flags != pack(ex)) begin
//          `logLevel( tb, 0, $format("TB: Flags mismatch. G:%h R:%h output G: %h , R: %h ", ex, _flags,_output,_out))
//	end
//---------------------------------------------------------------------------------------------------------------------------------- 
      read_index <= read_index + 1;
      if(read_index == `stim_size-1)
//      if(read_index == 100)	
        $finish(0);
    endrule

  endmodule
endpackage

