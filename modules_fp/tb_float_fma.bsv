/* 
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
  with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: Neel Gala
Email id: neelgala@gmail.com
Details:

--------------------------------------------------------------------------------------------------
*/
package tb_float_fma; 
  `include "Logger.bsv"
  import FIFO :: * ;
  import FIFOF :: * ;
  import SpecialFIFOs :: * ;
  import fpu_fma :: * ;
  import RegFile :: * ;
  import fpu_common :: * ;
//*******************************************************************
import Real              ::*;
import Vector            ::*;
import BUtils            ::*;
import DefaultValue      ::*;
import FShow             ::*;
import GetPut            ::*;
import ClientServer      ::*;
import FIFO              ::*;
import Divide            ::*;
import SquareRoot        ::*;
import FixedPoint        ::*;

//*******************************************************************
`ifdef FLEN64
  `define FLEN 64	
  `define a1 51
  `define a2 52
  `define a3 62
  `define a4 63
  `define MOD mk_fpu_fma_dp_instance

`else
  `define FLEN 32
  `define a1 22
  `define a2 23
  `define a3 30
  `define a4 31
  `define MOD mk_fpu_fma_sp_instance
`endif
  (*synthesize*)
  module mktb_float(Empty);
//***************************************************** fmaFP starts ***************************************************************************

 function Tuple2#(FloatingPoint#(e,m),Exception) fmaFP(Tuple4#(Maybe#(FloatingPoint#(e,m)),
		 FloatingPoint#(e,m),
		 FloatingPoint#(e,m),
		 RoundMode) operands)
		 provisos(
      Add#(e,2,ebits),
      Add#(m,1,mbits),
      Add#(m,5,m5bits),
      Add#(mbits,mbits,mmbits),
      // per request of bsc
      Add#(1, a__, mmbits),
      Add#(m, b__, mmbits),
      Add#(c__, TLog#(TAdd#(1, mmbits)), TAdd#(e, 1)),
      Add#(d__, TLog#(TAdd#(1, m5bits)), TAdd#(e, 1)),
      Add#(1, TAdd#(1, TAdd#(m, 3)), m5bits)
      );

      match { .mopA, .opB, .opC, .rmode } = operands;

      CommonState#(e,m) s = CommonState {
	 res: tagged Invalid,
	 exc: defaultValue,
	 rmode: rmode
	 };

      Bool acc = False;
      FloatingPoint#(e,m) opA = 0;

      if (mopA matches tagged Valid .opA_) begin
	 opA = opA_;
	 acc = True;
      end

      Int#(ebits) expB = isSubNormal(opB) ? fromInteger(minexp(opB)) : signExtend(unpack(unbias(opB)));
      Int#(ebits) expC = isSubNormal(opC) ? fromInteger(minexp(opC)) : signExtend(unpack(unbias(opC)));

      Bit#(mbits) sfdB = { getHiddenBit(opB), opB.sfd };
      Bit#(mbits) sfdC = { getHiddenBit(opC), opC.sfd };

      Bool sgnBC = opB.sign != opC.sign;
      Bool infBC = isInfinity(opB) || isInfinity(opC);
      Bool zeroBC = isZero(opB) || isZero(opC);
      Int#(ebits) expBC = expB + expC;

      if (isSNaN(opA)) begin
	 s.res = tagged Valid nanQuiet(opA);
	 s.exc.invalid_op = True;
      end
      else if (isSNaN(opB)) begin
	 s.res = tagged Valid nanQuiet(opB);
	 s.exc.invalid_op = True;
      end
      else if (isSNaN(opC)) begin
	 s.res = tagged Valid nanQuiet(opC);
	 s.exc.invalid_op = True;
      end
      else if (isQNaN(opA)) begin
	 s.res = tagged Valid opA;
      end
      else if (isQNaN(opB)) begin
	 s.res = tagged Valid opB;
      end
      else if (isQNaN(opC)) begin
	 s.res = tagged Valid opC;
      end
      else if ((isInfinity(opB) && isZero(opC)) || (isZero(opB) && isInfinity(opC)) || (isInfinity(opA) && infBC && (opA.sign != sgnBC))) begin
	 // product of zero and infinity or addition of opposite sign infinity
	 s.res = tagged Valid qnan();
	 s.exc.invalid_op = True;
      end
      else if (isInfinity(opA)) begin
	 s.res = tagged Valid opA;
      end
      else if (infBC) begin
	 s.res = tagged Valid infinity(sgnBC);
      end
      else if (isZero(opA) && zeroBC && (opA.sign == sgnBC)) begin
	 s.res = tagged Valid opA;
      end


      let sfdBC = primMul(sfdB, sfdC);


   // passthrough stage for multiply register retiming
 

   // normalize multiplication result
  

      FloatingPoint#(e,m) bc = defaultValue;
      Bit#(2) guardBC = ?;

      if (s.res matches tagged Invalid) begin
	 if (expBC > fromInteger(maxexp(bc))) begin
	    bc.sign = sgnBC;
	    bc.exp = maxBound - 1;
	    bc.sfd = maxBound;
	    guardBC = '1;

	    s.exc.overflow = True;
	    s.exc.inexact = True;
	 end
	 else if (expBC < (fromInteger(minexp_subnormal(bc))-2)) begin
	    bc.sign = sgnBC;
	    bc.exp = 0;
	    bc.sfd = 0;
	    guardBC = 0;

	    if (|sfdBC == 1) begin
	       guardBC = 1;
	       s.exc.underflow = True;
	       s.exc.inexact = True;
	    end
	 end
	 else begin
	    let shift1 = fromInteger(minexp(bc)) - expBC;
	    if (shift1 > 0) begin
	       // subnormal
	       Bit#(1) sfdlsb = |(sfdBC << (fromInteger(valueOf(mmbits)) - shift1));

	       sfdBC = sfdBC >> shift1;
	       sfdBC[0] = sfdBC[0] | sfdlsb;

	       bc.exp = 0;
	    end
	    else begin
	       bc.exp = cExtend(expBC + fromInteger(bias(bc)));
	    end

	    bc.sign = sgnBC;
	    let y3 = normalize(bc, sfdBC);
	    bc = tpl_1(y3);
	    guardBC = tpl_2(y3);
	    s.exc = s.exc | tpl_3(y3);
	 end
      end

      Int#(ebits) expA = isSubNormal(opA) ? fromInteger(minexp(opA)) : signExtend(unpack(unbias(opA)));
      expBC = isSubNormal(bc) ? fromInteger(minexp(bc)) : signExtend(unpack(unbias(bc)));

      Bit#(m5bits) sfdA = {1'b0, getHiddenBit(opA), opA.sfd, 3'b0};
      sfdBC = {1'b0, getHiddenBit(bc), bc.sfd, guardBC, 1'b0};

      Bool sub = opA.sign != bc.sign;

      Int#(ebits) exp = ?;
      Int#(ebits) shift = ?;
      Bit#(m5bits) x1 = ?;
      Bit#(m5bits) y1 = ?;
      Bool sgn = ?;

      if ((!acc) || (expBC > expA) || ((expBC == expA) && (sfdBC > sfdA))) begin
	 exp = expBC;
	 shift = expBC - expA;
	 x1 = sfdBC;
	 y1 = sfdA;
	 sgn = bc.sign;
      end
      else begin
	 exp = expA;
	 shift = expA - expBC;
	 x1 = sfdA;
	 y1 = sfdBC;
	 sgn = opA.sign;
      end

   
      if (s.res matches tagged Invalid) begin
	 if (shift < fromInteger(valueOf(m5bits))) begin
	    Bit#(m5bits) guard1;

	    guard1 = y1 << (fromInteger(valueOf(m5bits)) - shift);
	    y1 = y1 >> shift;
	    y1[0] = y1[0] | (|guard1);
	 end
	 else if (|y1 == 1) begin
	    y1 = 1;
	 end
      end

     
      let sum = x1 + y1;
      let diff = x1 - y1;

    

      FloatingPoint#(e,m) out = defaultValue;
      Bit#(2) guard = 0;

      if (s.res matches tagged Invalid) begin
	 Bit#(m5bits) sfd;

	 sfd = sub ? diff : sum;

	 out.sign = sgn;
	 out.exp = cExtend(exp + fromInteger(bias(out)));

	 let y2 = normalize(out, sfd);
	 out = tpl_1(y2);
	 guard = tpl_2(y2);
	 s.exc = s.exc | tpl_3(y2);
      end


      if (s.res matches tagged Valid .x) begin
	 out = x;
      end
      else begin
	 let y = round(s.rmode, out, guard);
	 out = tpl_1(y);
	 s.exc = s.exc | tpl_2(y);

	 // adjust sign for exact zero result
	 if (acc && isZero(out) && !s.exc.inexact && sub) begin
	    out.sign = (s.rmode == Rnd_Minus_Inf);
	 end
      end

   if(s.exc.overflow == True) begin
	Bool sign = False;
	Bit#(e) expo = 'hff;
	Bit#(m) man = 'b10000000000000000000000;
	let result = FloatingPoint{sign : sign,exp: expo,sfd: man};
	return tuple2(result,s.exc);   	
   end
   else if(s.exc.underflow == True) begin
	Bool sign = True;
	Bit#(e) expo = 'hff;
	Bit#(m) man = 'b10000000000000000000000;
	let result = FloatingPoint{sign : sign,exp: expo,sfd: man};
	return tuple2(result,s.exc);   	
   end
   else 
   return tuple2(canonicalize(out),s.exc);
		
   
   endfunction

//***************************************************** fmaFP ends *****************************************************************
    RegFile#(Bit#(`index_size) , Bit#(`entry_size)) stimulus <- mkRegFileLoad("input.txt", 0, `stim_size-1);
//    let fadd <- mk_fpu_add_sub_sp_instance;
    let fadd <- (`MOD);

    FIFOF#(Tuple2#(Bit#(`FLEN), Bit#(5))) ff_golden_output <- mkSizedFIFOF(2);

    Reg#(Bit#(`index_size)) read_index <- mkReg(0);
    Reg#(Bit#(`index_size)) golden_index <- mkReg(0);

    /*doc:rule: */
    rule rl_pick_stimulus_entry;
      
      let _e = stimulus.sub(read_index);
      Bit#(8) _flags = truncate(_e);
      _e = _e >> 8;
      Bit#(`FLEN) _output = truncate(_e);
      _e = _e >> `FLEN;
      Bit#(`FLEN) _inp3 = truncate(_e);
      _e = _e >> `FLEN;
      Bit#(`FLEN) _inp2 = truncate(_e);
      _e = _e >> `FLEN;
      Bit#(`FLEN) _inp1 = truncate(_e);

      let op1 = FloatingPoint{sign : unpack(_inp1[`a4]), exp: _inp1[`a3:`a2], sfd: _inp1[`a1:0]};
      let op2 = FloatingPoint{sign : unpack(_inp2[`a4]), exp: _inp2[`a3:`a2], sfd: _inp2[`a1:0]};
      let op3 = FloatingPoint{sign : unpack(_inp3[`a4]), exp: _inp3[`a3:`a2], sfd: _inp3[`a1:0]};
      `logLevel( tb, 0, $format("TB: Sending inputs[%d]: op1:%h op2:%h, op3:%h, output %h", read_index, op1, op2,op3,_output))

//      fadd.send(tuple4(tagged Valid op3, op1, op2, `rounding_mode));

//----------------------------------- fma function testing -------------------------------------------------------------------------
     match {.out,.ex} = fmaFP(tuple4(tagged Valid op3, op1, op2, `rounding_mode));
	Bit#(5) pack_flags = truncate(_flags);
     Bit#(`FLEN) _out = {pack(out.sign), out.exp, out.sfd};
	 if( _out != _output) begin
          `logLevel( tb, 0, $format("TB: Outputs mismatch[%d]. G:%h R:%h, flags G: %h , R: %h",read_index, _output, _out,pack_flags, pack(ex) ))
//          $finish(0);
        end
        else begin
//            `logLevel( tb, 0, $format("TB: Outputs match [%d], g: %h R: %h flags G: %h, R: %h", read_index,_output, _out,pack_flags,pack(ex)))
        end
        if( pack_flags != pack(ex)) begin
//          `logLevel( tb, 0, $format("TB: Flags mismatch. G:%h R:%h output G: %h , R: %h ", ex, _flags,_output,_out))
//          $finish(0);*/
	end
//---------------------------------------------------------------------------------------------------------------------------------- 
//      ff_golden_output.enq(tuple2(_output, truncate(_flags)));
      read_index <= read_index + 1;
      if(read_index == `stim_size-1)
        $finish(0);
    endrule

    /*doc:rule: */
/*    rule rl_check_output;
//      match {.valid,.out,.flags} = fadd.receive();
      let x = fadd.receive();
      let valid = x.valid;
      let out = x.value;
      let flags = x.ex;
      if(valid==1) begin
        let {e_out, e_flags} = ff_golden_output.first;
        Bit#(`FLEN) _out = {pack(out.sign), out.exp, out.sfd};
        ff_golden_output.deq;
        if( _out != e_out) begin
          `logLevel( tb, 0, $format("TB: Outputs mismatch[%d]. G:%h R:%h,flag G: %h, flag R: %h",golden_index, e_out, _out,e_flags, flags))
//          $finish(0);
        end
        else begin
            `logLevel( tb, 0, $format("TB: Outputs match [%d], g: %h R: %h flag G: %h, flag R: %h", golden_index,e_out, _out,e_flags,flags))
        end
        golden_index <= golden_index + 1;
        if( flags != unpack(e_flags)) begin
          `logLevel( tb, 0, $format("TB: Flags mismatch. G:%h R:%h output G: %h , R: %h ", e_flags, flags,e_out,_out))
//          $finish(0);
        end
      end
    endrule*/
  endmodule
endpackage

