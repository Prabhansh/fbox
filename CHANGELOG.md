# CHANGELOG

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.7.1] - 2020-06-23
## Fixed
- Setting fmin/fmax flags correctly

## [1.7.0] - 2020-06-20
## Fixed
- fma operand ordering fixed

## [1.6.9] - 2020-06-19
## Fixed
- fma operand ordering

## [1.6.8] - 2020-06-18
## Fixed
- fp classify

## [1.6.7] - 2020-06-11
## Fixed
- fp compare 

## [1.6.6] - 2020-05-25
## Fixed
- Makefile

## [1.6.5] - 2020-05-15
## Fixed
- sp->dp and dp->sp bug fix (flags)

## [1.6.4] - 2020-05-12
## Fixed
- enum: rounding mode ordering

## [1.6.3] - 2020-04-29
## Added
- sign injection to gitlab ci

## [1.6.2] - 2020-04-29
## Fixed
- gitlab ci

## [1.6.1] - 2020-04-29
## Fixed
- fixes for max mag rounding mode and fp multiplier flag mismatches

## [1.6.0] - 2020-04-15
## Added
- hierarchical fpu

## [1.5.4] - 2020-04-06
## Fixed 
- parameters

## [1.5.3] - 2020-04-31
## Added 
- module name fix for int to sp/dp and ci fix

## [1.5.2] - 2020-03-25
## Added 
- Number of stages parameter to interface for multicycle fp modules (added to int_to_fp modules)

## [1.5.1] - 2020-03-04
## Fixed
- Minor changes (display)

## [1.5.0] - 2020-03-04
## Fixed
- Changes to fp divider for timing closure.

## [1.4.4] - 2020-02-19
## Fixed
- Changes to Makefile (open bsc) and ci (adding other fp modules)

## [1.4.3] - 2020-02-06
## Fixed
- fixed some bugs in fma modules (from bluespec lib)

## [1.4.2] - 2020-01-09
## Fixed
- fixed ci typos to generate sp/dp conversion modules correctly

## [1.4.1] - 2020-01-08
## Fixed
- fixed Makefile.inc to generate add/sub verilog. Need to change this to generate all verilog once fpu.bsv is done.
- moved intefer_divider.bsv to obsolete

## [1.4.0] - 2020-01-08
## Fixed
- naming convention changes for fpu_convert (file with multiple modules)
- removed dead code from fpu_div_sqrt (now contains modules only 1 fp_divider and 1 fp_sqrt)
- removed unnecessary actionvalue from fpu_sign_injection
- changes for canonicalizing NaN outputs from every module in modules_fp
- changes to output structure of all modules in modules_fp - has only one ready bit output now

## [1.3.0] - 2019-12-31
## Fixed 
- removed verif components 
- updated ci to release verilog
- moved single-cycle-shakti design to obsolete

## [1.2.0] - 2019-12-19
## Fixed 
- gitlab ci
- updated verif test bench environment
- bsv testbench and corresponding Makefile fixed for more scalability and testing.

## [1.1.2] - 2019-12-18
## Fixed 
- fixed naming convention for files with one module
- removed dead code in fpu_multiplier.bsv
- corresponding instance name change in tb_float.bsv

## [1.1.1] - 2019-12-17
## Added
- added and verified changes to tb_float.bsv to accomodate for adding valid bit to output for fp units

## [1.1.0] - 2019-12-16
## Added
- renamed floating_point.bsv to fpu_common.bsv and moved dead code to fpu_common_backup.bsv
- added support for FMV.W.X and FMV.X.W
- added valid bit to output for all units except div/sqrt.
- added fpu_sign_injection.bsv.
## Fixed
- fixed denormal support errors in fpu_fma.bsv
- fixed soft-float build for riscv. Imported specialize files from spike
- enabled denormal support by default for bsv_test_bench

## [1.0.3] - 2019-11-29
## Added
- basic synth support script for sp_add_sub
- basic bsv based test-bench to run softfloat generated tests.
## Removed
- removed redundant modulesFP folder
## Fixed
- cicd script has been fixed as well

## [1.0.2] - 2019-11-27
## Added
- interface changes for add_sub
- modules for divider and sqrt functions

## [1.0.1] - 2019-11-20
## Added
- verification setup

